#14. Multithreading. Ефективність використання
## Тема
- Вимірювання часу паралельних та послідовних обчислень.
- Демонстрація ефективності паралельної обробки.



## 1. Вимоги
1. Забезпечити вимірювання часу паралельної обробки елементів контейнера за допомогою розроблених раніше методів.




2. Додати до алгоритмів штучну затримку виконання для кожної ітерації циклів поелементної обробки контейнерів, щоб загальний час обробки був декілька секунд.


3. Реалізувати послідовну обробку контейнера за допомогою методів, що використовувались для паралельної обробки та забезпечити вимірювання часу їх роботи.

4. Порівняти час паралельної і послідовної обробки та зробити висновки про ефективність розпаралелювання:

- результати вимірювання часу звести в таблицю;
- обчислити та продемонструвати у скільки разів паралельне виконання швидше послідовного.





### 1.1 Розробник
- Москаль Кирило Артемович
- КН-921в
- 12 варіант

### 1.2 Загальне завдання
- Розробити прогрму 
- Оформити роботу

### 2.1 Засоби ООП
- Java code convention
- JDK:
- ООП
### 2.2 Ієрархія та структура класів
- 1. Main

### 2.3 Важливі фрагменти програми:
 
~~~java
public static void main(String[] args) {
        List<Integer> list = generateRandomList(NUM_ELEMENTS);

        long startTime = System.currentTimeMillis();
        List<Integer> sequentialResult = processSequentially(list);
        long endTime = System.currentTimeMillis();
        long sequentialTime = endTime - startTime;

        startTime = System.currentTimeMillis();
        List<Integer> parallelResult = processInParallel(list);
        endTime = System.currentTimeMillis();
        long parallelTime = endTime - startTime;

        System.out.println("Sequential Time: " + sequentialTime + " ms");
        System.out.println("Parallel Time: " + parallelTime + " ms");

        System.out.println("Speedup: " + (double) sequentialTime / parallelTime);

        boolean resultsEqual = sequentialResult.equals(parallelResult);
        System.out.println("Results equal: " + resultsEqual);
    }
~~~

## Варіанти використання
Демонстрація роботи потоків
## Висновки
На цій лабораторній роботі покращив навички роботи з потоками