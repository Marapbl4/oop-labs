# 7. Об'єктно-орієнтована декомпозиція
## Тема
- Використання об'єктно-орієнтованого підходу для розробки об'єкта предметної (прикладної) галузі.

## 1. Вимоги
- Використовуючи об'єктно-орієнтований аналіз, реалізувати класи для представлення сутностей відповідно прикладної задачі - domain-об'єктів.

- Забезпечити та продемонструвати коректне введення та відображення кирилиці.

- Продемонструвати можливість управління масивом domain-об'єктів.
### 1.1 Розробник
- Москаль Кирило Артемович
- КН-921в
- 12 варіант

### 1.2 Загальне завдання
- Розробити прогрму 
- Оформити роботу


### 1.3 Задача
див. у 1
  
### 2.1 Засоби ООП
- Java code convention
- JDK:
- Ітератор
### 2.2 Ієрархія та структура класів
- 1. Main
- 2. AudioLibrary

### 2.3 Важливі фрагменти програми:
- Занесення у класс інформацію
~~~java
   public void setName(final String value) {
        this.name = value;
    }

    public void setGenre(final String value) {
    	this.genre = value;
    }
    public void setArtist(final String value) {
    	this.artist = value;
    }
    public void setDateOfCreation(final String value) {
    	this.dateOfCreation = value;
    }
    public void setDuration(final String value) {
    	this.duration = value;
    }
    public void setDataFormat(final String value) {
    	this.dataFormat = value;
    }
~~~
- Отримання інформації
~~~java
        public String getName() {
        return name;
    }
    public String getGenre() {
        return genre;
    }
    public String getArtist() {
    	return artist;
    }
    public String getDateOfCreation() {
    	return dateOfCreation;
    }
    public String getDuration() {
        return duration;
    }
    public String getDataFormat() {
    	return dataFormat;
    }
~~~
## Варіанти використання
Демонстрація роботи ООП 
## Висновки
На цій лабораторній роботі навчились працювати з ооп

