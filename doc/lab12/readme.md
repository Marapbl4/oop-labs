# 12. Регулярні вирази. Обробка тексту∗
## Тема
- Ознайомлення з принципами використання регулярних виразів для обробки тексту.



## 1. Вимоги
1. Використовуючи програми рішень попередніх задач, продемонструвати ефективне (оптимальне) використання регулярних виразів при вирішенні прикладної задачі.


2. Передбачити можливість незначної зміни умов пошуку.

3. Продемонструвати розроблену функціональність в діалоговому та автоматичному режимах.




### 1.1 Розробник
- Москаль Кирило Артемович
- КН-921в
- 12 варіант

### 1.2 Загальне завдання
- Розробити прогрму 
- Оформити роботу


### 1.3 Задача
Аудіотека. Знайти аудіозаписи, що стиснуті з мінімальними втратами або без втрат аудіоданих (визначається форматом), в назві або тексті яких згадується святкування Нового Року.


### 2 Засоби ООП
- Java code convention
- JDK:
- ООП
### 2.1 Ієрархія та структура класів
- 1. Main
- 1. AudioLibrary
- 1. AudioLibraryContainer

### 2.3 Важливі фрагменти програми:
 
~~~java
    public ArrayList<AudioLibrary> getsongs() {
        return songs;
    }
    public ArrayList<AudioLibrary> filterByDataFormat(String format) {
        ArrayList<AudioLibrary> filteredList = new ArrayList<AudioLibrary>();
        for (AudioLibrary AudioLibrary : songs) {
            if (AudioLibrary.getDataFormat() == format) {
                filteredList.add(AudioLibrary);
            }
        }
        return filteredList;
    }
    public ArrayList<AudioLibrary> filterBySongName(String SongName) {
        ArrayList<AudioLibrary> filteredList = new ArrayList<AudioLibrary>();
        for (AudioLibrary AudioLibrary : songs) {
            if (AudioLibrary.getPosition() .equalsIgnoreCase(SongName)) {
                filteredList.add(AudioLibrary);
            }
        }
        return filteredList;
    }
~~~

## Варіанти використання
Демонстрація роботи потоків 
## Висновки
На цій лабораторній роботі навчились працювати з потоками