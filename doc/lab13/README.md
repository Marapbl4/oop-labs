# 12. Паралельне виконання. Multithreading
## Тема
- Ознайомлення з моделлю потоків Java.



## 1. Вимоги
1. Використовуючи програми рішень попередніх задач, продемонструвати можливість паралельної обробки елементів контейнера: створити не менше трьох додаткових потоків, на яких викликати відповідні методи обробки контейнера.

2.Забезпечити можливість встановлення користувачем максимального часу виконання (таймаута) при закінченні якого обробка повинна припинятися незалежно від того знайдений кінцевий результат чи ні.

3.Для паралельної обробки використовувати алгоритми, що не змінюють початкову колекцію.

4.Кількість елементів контейнера повинна бути досить велика, складність алгоритмів обробки колекції повинна бути зіставна, а час виконання приблизно однаковий, наприклад:

- пошук мінімуму або максимуму;
- обчислення середнього значення або суми;
- підрахунок елементів, що задовольняють деякій умові;
- відбір за заданим критерієм;
- власний варіант, що відповідає обраній прикладної області.



### 1.1 Розробник
- Москаль Кирило Артемович
- КН-921в
- 12 варіант

### 1.2 Загальне завдання
- Розробити прогрму 
- Оформити роботу

### 2 Засоби ООП
- Java code convention
- JDK:
- ООП
### 2.1 Ієрархія та структура класів
- 1. Main
- 2. AudioLibrary
- 3. AudioLibraryContainer

### 2.3 Важливі фрагменти програми:
 
~~~java
    public static ArrayList<AudioLibrary> filterBySongNameInParallel(AudioLibraryContainer container, String SongName) throws InterruptedException, ExecutionException {
        ExecutorService executor = Executors.newFixedThreadPool(3);

        List<Callable<ArrayList<AudioLibrary>>> tasks = new ArrayList<>();
        tasks.add(() -> container.filterBySongName(SongName));
        tasks.add(() -> container.filterBySongName(SongName));
        tasks.add(() -> container.filterBySongName(SongName));

        List<Future<ArrayList<AudioLibrary>>> results = executor.invokeAll(tasks);

        ArrayList<AudioLibrary> filteredSong = new ArrayList<>();
        for (Future<ArrayList<AudioLibrary>> result : results) {
            filteredSong.addAll(result.get());
        }

        executor.shutdown();

        return filteredSong;
    }
~~~

## Варіанти використання
Демонстрація роботи потоків 
## Висновки
На цій лабораторній роботі навчились працювати з потоками