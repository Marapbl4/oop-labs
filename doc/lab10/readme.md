# 10. Обробка параметризованих контейнерів
## Тема
- Розширення функціональності параметризованих класів.


## 1. Вимоги
1. Використовуючи програму рішення завдання лабораторної роботи №9:

2. Продемонструвати розроблену функціональність (створення, управління та обробку власних контейнерів) в діалоговому та автоматичному режимах.

2.1 Автоматичний режим виконання програми задається параметром командного рядка -auto. Наприклад, java ClassName -auto.

2.2 В автоматичному режимі діалог з користувачем відсутній, необхідні данні генеруються, або зчитуються з файлу.

3. Забороняється використання алгоритмів з Java Collections Framework.

### 1.1 Розробник
- Москаль Кирило Артемович
- КН-921в
- 12 варіант

### 1.2 Загальне завдання
- Розробити прогрму 
- Оформити роботу


### 1.3 Задача
Аудіотека. Сортування за назвою композиції, за виконавцем, за жанром.

## 2. Вивід у консоль
відсутній
### 2.1 Засоби ООП
- Java code convention
- JDK:
- ООП
### 2.2 Ієрархія та структура класів
- 1. Main
- 2. AudioLibrary
- 3. AudioLibraryContainer

### 2.3 Важливі фрагменти програми:
- 
~~~java
public <T extends Comparable<T>> void sortBySongName(List<AudioLibrary> list, boolean ascending) {
        list.sort((v1, v2) -> {
            int result = v1.getName().compareTo(v2.getName());
            return ascending ? result : -result;
        });
    }

    
    public <T extends Comparable<T>> void sortByArtist(List<T> songs, boolean ascending) {
    	list = null;
		list.sort((v1, v2) -> {
            int result = v1.getArtist().compareTo(v2.getArtist());
            return ascending ? result : -result;
        });
    }

    
    public <T extends Comparable<T>> void sortByGenre(List<T> songs, boolean ascending) {
    	list.sort((v1, v2) -> {
            int result = v1.getGenre().compareTo(v2.getGenre());
            return ascending ? result : -result;
        });
    }
~~~

## Варіанти використання
Демонстрація роботи параметризованих контейнерів 
## Висновки
На цій лабораторній роботі навчились працювати з параметризованими контейнерами