# 9. Параметризація в Java
## Тема
- Вивчення принципів параметризації в Java.
- Розробка параметризованих класів та методів.


## 1. Вимоги
1. Створити власний клас-контейнер, що параметризується (Generic Type), на основі зв'язних списків для реалізації колекції domain-об’єктів лабораторної роботи №7.

2. Для розроблених класів-контейнерів забезпечити можливість використання їх об'єктів у циклі foreach в якості джерела даних.

3. Забезпечити можливість збереження та відновлення колекції об'єктів: 
  3.1 за допомогою стандартної серіалізації;
  3.2 не використовуючи протокол серіалізації.

4. Продемонструвати розроблену функціональність: створення контейнера, додавання елементів, видалення елементів, очищення контейнера, перетворення у масив, перетворення у рядок, перевірку на наявність елементів

5. Забороняється використання контейнерів (колекцій) з Java Collections Framework.


### 1.1 Розробник
- Москаль Кирило Артемович
- КН-921в
- 12 варіант

### 1.2 Загальне завдання
- Розробити програму 
- Оформити роботу

### 2 Засоби ООП
- Java code convention
- JDK:
- ООП
### 2.1 Ієрархія та структура класів
- 1. Main
- 2. AudioLibrary
- 3. AudioLibraryContainer

### 2.2 Важливі фрагменти програми:
- 
~~~java
public class AudioLibraryContainer implements Serializable {
    
    private static final long serialVersionUID = 1L; 
    
    private List<AudioLibrary> library; 
    
    
    public AudioLibraryContainer() {
        songs = new ArrayList<>();
    }
    
    
    public void add(AudioLibrary song) {
        songs.add(song);
    }
    
    
    public void remove(AudioLibrary song) {
        songs.remove(song);
    }
    
    
    public AudioLibrary get(int index) {
        return songs.get(index);
    }
    
    
    public List<AudioLibrary> getAll() {
        return new ArrayList<>(songs);
    }
    
    
    public int size() {
        return songs.size();
    }
    
    
    public boolean isEmpty() {
        return songs.isEmpty();
    }
    
    
    public void clear() {
        songs.clear();
    } 
}
~~~

## Варіанти використання
Демонстрація роботи параметризації 
## Висновки
На цій лабораторній роботі навчились працювати з параметризацією