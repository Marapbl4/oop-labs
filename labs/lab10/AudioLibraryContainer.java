package lab10;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AudioLibraryContainer implements Serializable {
    
     
public class AudioLibraryContainer implements Serializable {
    
    private static final long serialVersionUID = 1L; 
    
    private List<AudioLibrary> library; 
    
    
    public AudioLibraryContainer() {
        songs = new ArrayList<>();
    }
    
    
    public void add(AudioLibrary song) {
        songs.add(song);
    }
    
    
    public void remove(AudioLibrary song) {
        songs.remove(song);
    }
    
    
    public AudioLibrary get(int index) {
        return songs.get(index);
    }
    
    
    public List<AudioLibrary> getAll() {
        return new ArrayList<>(songs);
    }
    
    
    public int size() {
        return songs.size();
    }
    
    
    public boolean isEmpty() {
        return songs.isEmpty();
    }
    
    
    public void clear() {
        songs.clear();
    }

        
    public <T extends Comparable<T>> void sortBySongName(List<AudioLibrary> list, boolean ascending) {
        list.sort((v1, v2) -> {
            int result = v1.getName().compareTo(v2.getName());
            return ascending ? result : -result;
        });
    }

    
    public <T extends Comparable<T>> void sortByArtist(List<T> songs, boolean ascending) {
    	list = null;
		list.sort((v1, v2) -> {
            int result = v1.getArtist().compareTo(v2.getArtist());
            return ascending ? result : -result;
        });
    }

    
    public <T extends Comparable<T>> void sortByGenre(List<T> songs, boolean ascending) {
    	list.sort((v1, v2) -> {
            int result = v1.getGenre().compareTo(v2.getGenre());
            return ascending ? result : -result;
        }); 
}
    
    
}