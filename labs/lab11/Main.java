package lab11;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Scanner;

public class Main {
    
    private static final Pattern NAME_PATTERN = Pattern.compile("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$");
   
    private static final Pattern EMAIL_PATTERN = Pattern.compile("^[\\w.-]+@[\\w.-]+\\.[a-zA-Z]{2,6}$");

    public static void main(String[] args) {
    	try (Scanner scanner = new Scanner(System.in)) {
			String name = scanner.nextLine();
			String email = scanner.nextLine();
			
			boolean validName = validateName(name);
			boolean validEmail = validateEmail(email);

			System.out.println("Name: " + name + " is " + (validName ? "valid" : "invalid"));
			System.out.println("Email: " + email + " is " + (validEmail ? "valid" : "invalid"));
		}   
       }

    
    public static boolean validateName(String name) {
        Matcher matcher = NAME_PATTERN.matcher(name);
        return matcher.matches();
    }


    public static boolean validateEmail(String email) {
        Matcher matcher = EMAIL_PATTERN.matcher(email);
        return matcher.matches();
    }
}