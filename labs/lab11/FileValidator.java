package lab11;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileValidator {
    // Патерн для перевірки імен
    private static final Pattern NAME_PATTERN = Pattern.compile("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$");
    // Патерн для перевірки електронної пошти
    private static final Pattern EMAIL_PATTERN = Pattern.compile("^[\\w.-]+@[\\w.-]+\\.[a-zA-Z]{2,6}$");

    public static void main(String[] args) {
        try {
            File inputFile = new File("input.txt");
            Scanner scanner = new Scanner(inputFile);

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] parts = line.split(",");

                String name = parts[0];
                String email = parts[1];

                boolean validName = validateName(name);
                boolean validEmail = validateEmail(email);

                System.out.println("Name: " + name + " is " + (validName ? "valid" : "invalid"));
                System.out.println("Email: " + email + " is " + (validEmail ? "valid" : "invalid"));
            }

            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }
    }

    // Метод для перевірки імен
    public static boolean validateName(String name) {
        Matcher matcher = NAME_PATTERN.matcher(name);
        return matcher.matches();
    }

    // Метод для перевірки електронної пошти
    public static boolean validateEmail(String email) {
        Matcher matcher = EMAIL_PATTERN.matcher(email);
        return matcher.matches();
    }
}