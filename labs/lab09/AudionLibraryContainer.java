package lab09;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AudioLibraryContainer implements Serializable {
    
     
public class AudioLibraryContainer implements Serializable {
    
    private static final long serialVersionUID = 1L; 
    
    private List<AudioLibrary> library; 
    
    
    public AudioLibraryContainer() {
        songs = new ArrayList<>();
    }
    
    
    public void add(AudioLibrary song) {
        songs.add(song);
    }
    
    
    public void remove(AudioLibrary song) {
        songs.remove(song);
    }
    
    
    public AudioLibrary get(int index) {
        return songs.get(index);
    }
    
    
    public List<AudioLibrary> getAll() {
        return new ArrayList<>(songs);
    }
    
    
    public int size() {
        return songs.size();
    }
    
    
    public boolean isEmpty() {
        return songs.isEmpty();
    }
    
    
    public void clear() {
        songs.clear();
    } 
}
    
    
}