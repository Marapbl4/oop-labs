package lab09;



public class AudioLibrary implements java.io.Serializable {

	
	private String name;
    private String genre;
    private String artist;
    private String dateOfCreation;
    private String duration;
    private String dataFormat;
    
    public AudioLibrary(String name, String genre, String artist, String dateOfCreation, String duration, String dataFormat) {
    	this.name = name;
    	this.genre = genre;
    	this.artist = artist;
    	this.dateOfCreation = dateOfCreation;
    	this.duration = duration;
    	this.dataFormat = dataFormat;
    }
    
    public String getDescription() {
        String description = "Song name: " + name +
                "\nArtist: " + artist +
                "\nGenre: " + genre +
                "\nDate of creation: " + dateOfCreation +
                "\nDuration: " + duration +
                "\nData Format: " + dataFormat;
        
        return description;
    }
    
    public String getName() {
        return name;
    }
    public String getGenre() {
        return genre;
    }
    public String getArtist() {
    	return artist;
    }
    public String getDateOfCreation() {
    	return dateOfCreation;
    }
    public String getDuration() {
        return duration;
    }
    public String getDataFormat() {
    	return dataFormat;
    }

    
    public void setName(final String value) {
        this.name = value;
    }

    public void setGenre(final String value) {
    	this.genre = value;
    }
    public void setArtist(final String value) {
    	this.artist = value;
    }
    public void setDateOfCreation(final String value) {
    	this.dateOfCreation = value;
    }
    public void setDuration(final String value) {
    	this.duration = value;
    }
    public void setDataFormat(final String value) {
    	this.dataFormat = value;
    }
    
    public String audioLibrary() {
    	return "Name: " + name  + "\ngenre: " + genre + "\nartist " + artist + "\ndateOfCreation " + dateOfCreation + "\nduration" + duration + "\ndataFormat" + dataFormat;
    }
}