package lab08;

public class AudioLibrarys implements java.io.Serializable {

	private String name = null;
    private String genre = null;
    private String artist = null;
    private String dateOfCreation = null;
    private String duration = null;
    private String dataFormat = null;
    
    public String getName() {
        return name;
    }
    public String getGenre() {
        return genre;
    }
    public String getArtist() {
    	return artist;
    }
    public String getDateOfCreation() {
    	return dateOfCreation;
    }
    public String getDuration() {
        return duration;
    }
    public String getDataFormat() {
    	return dataFormat;
    }

    
    public void setName(final String value) {
        this.name = value;
    }

    public void setGenre(final String value) {
    	this.genre = value;
    }
    public void setArtist(final String value) {
    	this.artist = value;
    }
    public void setDateOfCreation(final String value) {
    	this.dateOfCreation = value;
    }
    public void setDuration(final String value) {
    	this.duration = value;
    }
    public void setDataFormat(final String value) {
    	this.dataFormat = value;
    }
    
    public String audioLibrary() {
    	return "Name: " + name  + "\ngenre: " + genre + "\nartist " + artist + "\ndateOfCreation " + dateOfCreation + "\nduration" + duration + "\ndataFormat" + dataFormat;
    }
}