package lab16;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class FileValidatorTest {

    @Test
    @DisplayName("Should validate name")
    public void shouldValidateName() {
        Assertions.assertTrue(FileValidator.validateName("John Doe"));
        Assertions.assertTrue(FileValidator.validateName("John O'Connor"));
        Assertions.assertFalse(FileValidator.validateName("J0hn"));
        Assertions.assertFalse(FileValidator.validateName("JohnDoe123"));
    }

    @Test
    @DisplayName("Should validate email")
    public void shouldValidateEmail() {
        Assertions.assertTrue(FileValidator.validateEmail("test@example.com"));
        Assertions.assertTrue(FileValidator.validateEmail("test123@example.com"));
        Assertions.assertTrue(FileValidator.validateEmail("test-123@example.com"));
        Assertions.assertTrue(FileValidator.validateEmail("test.123@example.com"));
        Assertions.assertFalse(FileValidator.validateEmail("test@example"));
        Assertions.assertFalse(FileValidator.validateEmail("test@.com"));
        Assertions.assertFalse(FileValidator.validateEmail("test@example."));
    }
}