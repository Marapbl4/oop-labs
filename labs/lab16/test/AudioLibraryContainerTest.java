package lab16;

import java.util.Arrays;
import java.util.List;

public class Main {
	
	public static void main(String[] args) {
	    AudioLibraryContainer container = new AudioLibraryContainer();
	    
	    AudioLibrary song1 = new AudioLibrary("Paint It, Black", "Rock", "The Rolling Stones", "07.05.1966", "3:46", "mp3");
	    container.add(song1);
	    
	    AudioLibrary song2 = new AudioLibrary("TABOO", "Rap", "Denzel Curry", "27.07.2018", "3:18", "mp3");
	    
	    container.add(song2);
	    
	    
	    AudioLibrary song = container.get(0);
	    System.out.println(song.getName());
	    
	   
	    List<AudioLibrary> songs = container.getAll();
	    for (AudioLibrary v : songs) {
	        System.out.println(v.getName());
	    }
	    
	    
	    container.remove(song);
	    
	    
	    System.out.println(container.isEmpty());
	    
	    // clear container
	    container.clear();
	}
}