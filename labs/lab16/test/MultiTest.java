package lab16;


import org.junit.Assert;
import org.junit.Test;
import java.util.ArrayList;



public class MultiTest {

    @Test
    public void testAddSong() {
        AudioLibraryContainer container = new AudioLibraryContainer();
        AudioLibrary song = new AudioLibrary("Paint It, Black", "The Rolling Stones", "Rock");
        container.addSong(song);
        ArrayList<AudioLibrary> songs = container.getSong();
        Assert.assertEquals(1, songs.size());
        Assert.assertTrue(songs.contains(song));
    }

    @Test
    public void testRemoveSong() {
        AudioLibraryContainer container = new AudioLibraryContainer();
        AudioLibrary song = new AudioLibrary("Paint It, Black", "The Rolling Stones", "Rock");
        container.addSong(song);
        container.removeSong(song);
        ArrayList<AudioLibrary> songs = container.getSongs();
        Assert.assertEquals(0, songs.size());
        Assert.assertFalse(songs.contains(song));
    }

    @Test
    public void testFilterByGenre() {
        AudioLibraryContainer container = new AudioLibraryContainer();
        AudioLibrary song1 = new AudioLibrary("Paint It, Black", "The Rolling Stones", "Rock");
        AudioLibrary song2 = new AudioLibrary("TABOO", "Denzel Curry", "Rap");
        container.addSong(song1);
        container.addSong(song2);
        ArrayList<AudioLibrary> filteredList = container.FilterByGenre("Rock");
        Assert.assertEquals(1, filteredList.size());
        Assert.assertTrue(filteredList.contains(song1));
    }

    @Test
    public void testFilterBySongName() {
        AudioLibraryContainer container = new AudioLibraryContainer();
        AudioLibrary song1 = new AudioLibrary("Paint It, Black", "The Rolling Stones", "Rock");
        AudioLibrary song2 = new AudioLibrary("TABOO", "Denzel Curry", "Rap");
        container.addSong(song1);
        container.addSong(song2);
        ArrayList<AudioLibrary> filteredList = container.filterBySongName("TABOO");
        Assert.assertEquals(1, filteredList.size());
        Assert.assertTrue(filteredList.contains(AudioLibrary1));
    }
}