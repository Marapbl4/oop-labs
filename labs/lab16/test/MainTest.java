package lab16;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

public class MainTest {

    @Test
    void testGenerateRandomList() {
        List<Integer> list = Main.generateRandomList(10);
        assertEquals(10, list.size());
    }

    @Test
    void testProcessSequentially() {
        List<Integer> list = new ArrayList<>(List.of(1, 2, 3, 4, 5));
        List<Integer> expected = new ArrayList<>(List.of(2, 4, 6, 8, 10));
        List<Integer> result = Main.processSequentially(list);
        assertEquals(expected, result);
    }

    @Test
    void testProcessInParallel() {
        List<Integer> list = new ArrayList<>(List.of(1, 2, 3, 4, 5));
        List<Integer> expected = new ArrayList<>(List.of(2, 4, 6, 8, 10));
        List<Integer> result = Main.processInParallel(list);
        assertEquals(expected, result);
    }
}