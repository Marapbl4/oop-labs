package lab13;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
    
        AudioLibraryContainer AudioLibraryContainer = new AudioLibraryContainer();
        AudioLibraryContainer.addAudioLibrary(new AudioLibrary("Happy New Year", "Holidays song", "ABBA", "15.12.1980", "3:46", "FLAC" ));
        AudioLibraryContainer.addAudioLibrary(new AudioLibrary("Shape of You", "Pop", "Ed Sheeran", "06.01.2017", "3:55", "mp3" ));
        AudioLibraryContainer.addAudioLibrary(new AudioLibrary("I Don't Want to Set the World on Fire", "Jazz", "The Ink Spot", "1955", "3:00", "wav" ));
        AudioLibraryContainer.addAudioLibrary(new AudioLibrary("Starboy", "Pop", "The Weeknd", "19.04.2018", "3:51", "FLAC" ));


      ArrayList<AudioLibrary> filteredList = AudioLibraryContainer.filterByDataFormat(FLAC);

        for (AudioLibrary AudioLibrary : filteredList) {
            AudioLibrary.displayAudioLibrary();
            System.out.println();
        }
        
        ArrayList<AudioLibrary> filteredSong = AudioLibraryContainer.filterBySongName("New Year");

        for (AudioLibrary AudioLibrary : filteredSong) {
            AudioLibrary.displayAudioLibrary();
            System.out.println();
        }
    }
    public static ArrayList<AudioLibrary> filterBySongNameInParallel(AudioLibraryContainer container, String SongName) throws InterruptedException, ExecutionException {
        ExecutorService executor = Executors.newFixedThreadPool(3);

        List<Callable<ArrayList<AudioLibrary>>> tasks = new ArrayList<>();
        tasks.add(() -> container.filterBySongName(SongName));
        tasks.add(() -> container.filterBySongName(SongName));
        tasks.add(() -> container.filterBySongName(SongName));

        List<Future<ArrayList<AudioLibrary>>> results = executor.invokeAll(tasks);

        ArrayList<AudioLibrary> filteredSong = new ArrayList<>();
        for (Future<ArrayList<AudioLibrary>> result : results) {
            filteredSong.addAll(result.get());
        }

        executor.shutdown();

        return filteredSong;
    }
}