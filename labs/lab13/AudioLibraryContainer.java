package lab12;

import java.util.ArrayList;

public class AudioLibraryContainer {
    private ArrayList<AudioLibrary> songs;

    public AudioLibraryContainer() {
        songs = new ArrayList<>();
    }

    public void addAudioLibrary(AudioLibrary AudioLibrary) {
        songs.add(AudioLibrary);
    }

    public void removeAudioLibrary(AudioLibrary AudioLibrary) {
        songs.remove(AudioLibrary);
    }

    public ArrayList<AudioLibrary> getsongs() {
        return songs;
    }
    public ArrayList<AudioLibrary> filterByDataFormat(String format) {
        ArrayList<AudioLibrary> filteredList = new ArrayList<AudioLibrary>();
        for (AudioLibrary AudioLibrary : songs) {
            if (AudioLibrary.getDataFormat() == format) {
                filteredList.add(AudioLibrary);
            }
        }
        return filteredList;
    }
    public ArrayList<AudioLibrary> filterBySongName(String SongName) {
        ArrayList<AudioLibrary> filteredList = new ArrayList<AudioLibrary>();
        for (AudioLibrary AudioLibrary : songs) {
            if (AudioLibrary.getPosition() .equalsIgnoreCase(SongName)) {
                filteredList.add(AudioLibrary);
            }
        }
        return filteredList;
    }


}