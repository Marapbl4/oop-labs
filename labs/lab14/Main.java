package lab14;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
    private static final int NUM_ELEMENTS = 1000;

    public static void main(String[] args) {
        List<Integer> list = generateRandomList(NUM_ELEMENTS);

        long startTime = System.currentTimeMillis();
        List<Integer> sequentialResult = processSequentially(list);
        long endTime = System.currentTimeMillis();
        long sequentialTime = endTime - startTime;

        startTime = System.currentTimeMillis();
        List<Integer> parallelResult = processInParallel(list);
        endTime = System.currentTimeMillis();
        long parallelTime = endTime - startTime;

        System.out.println("Sequential Time: " + sequentialTime + " ms");
        System.out.println("Parallel Time: " + parallelTime + " ms");

        System.out.println("Speedup: " + (double) sequentialTime / parallelTime);

        boolean resultsEqual = sequentialResult.equals(parallelResult);
        System.out.println("Results equal: " + resultsEqual);
    }

    private static List<Integer> generateRandomList(int numElements) {
        Random rand = new Random();
        List<Integer> list = new ArrayList<>(numElements);
        for (int i = 0; i < numElements; i++) {
            list.add(rand.nextInt(1000));
        }
        return list;
    }

    private static List<Integer> processSequentially(List<Integer> list) {
        List<Integer> result = new ArrayList<>(list.size());
        for (Integer i : list) {
            sleep(5);
            result.add(i * 2);
        }
        return result;
    }

    private static List<Integer> processInParallel(List<Integer> list) {
        List<Integer> result = new ArrayList<>(list.size());
        list.parallelStream().forEach(i -> {
            sleep(5);
            result.add(i * 2);
        });
        return result;
    }

    private static void sleep(int duration) {
        try {
            Thread.sleep(duration);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}