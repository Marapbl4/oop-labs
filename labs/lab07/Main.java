package lab07;



public class Main {

	public static void main(final String[] arguments) {
        final AudioLibrary audioLibrary = new AudioLibrary();
             
        audioLibrary.setName("Love Sosa");
        audioLibrary.setGenre("Rap");
        audioLibrary.setArtist("Chief Keef");
        audioLibrary.setDateOfCreation("2012 рік");
        audioLibrary.setDuration("4:06");
        audioLibrary.setDataFormat("mp3");
        
        System.out.print(audioLibrary.getName() 
        		+ "\n" 
        		+ audioLibrary.getGenre() 
        		+ "\n" 
        		+ audioLibrary.getArtist()
        		+ "\n" 
        		+ audioLibrary.getDateOfCreation() 
        		+ "\n" 
        		+ audioLibrary.getDuration()
        		+ "\n"
        		+ audioLibrary.getDataFormat());
    }

}