package lab02;
import java.util.Random;

/**
 * Завдання: Знайти суму цифр заданого цілого числа.
 *
 * @author Moskal Kyrylo KN-921B
 *
 * @Version 1.0
 */


public class Main {
	 /** An instance of a class Random */
    static Random random = new Random();
    
    /**
     * Finding the sum of the digits in a number
     *
     * @param value the number in which to calculate the sum of the digits
     * @return the sum of digits
     */
    static int findSumNumber(int number) {
        if(number < 0) 
        	number *= -1;
        int sumNumber = 0;
        int digit = 0;
        while (number != 0) {
            digit = number%10;
            sumNumber = sumNumber + digit;
            number = number/10;
        }
        return sumNumber;
    }
    
    /**
     * Output variables to the console.
	 * @param i cycle number one unit added to house
	 * @param number number in that we are looking for a sum digit
	 * @param sumNumber result of finding sum of digit
     */

    static void print(int i, int number, int sumNumber) {
    	System.out.println( "\n___________________________________________________\n");
        System.out.println("|#" + (i+1) +"| | Число: " + number + "| | Сума цифр: " + sumNumber + "|");
        System.out.println("___________________________________________________");
    }
    
    /**
     * Program Entry Point
     *
     * @param args command line parameters
     */

	public static void main(String... args) {
		for(int i = 0; i < 10; i++){
            int number = random.nextInt();
            int sumNumber = findSumNumber(number);
            
            print(i, number, sumNumber);
        }
		
	}

}